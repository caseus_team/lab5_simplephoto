package graph;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.image.BufferedImage;

@SuppressWarnings("serial")
public class BaseButton extends JButton {
	
	public static enum ButtonStyle {
		ORANGE(
			Color.decode("#eb861d"),
			Color.decode("#eeeeee"),
			Color.decode("#f39d43"), Color.decode("#f8aa58"), Color.decode("#f49635")
		),
		VK(
			Color.decode("#5f6f96"),
			Color.decode("#eeeeee"),
			Color.decode("#6f7fa2"), Color.decode("#6888ad"), Color.decode("#5779a1")
		),
		GRAY(
			Color.decode("#727272"),
			Color.decode("#cccccc"),
			Color.decode("#575757"), Color.decode("#656565"), Color.decode("#727272")	
		),
		BASE(
			Color.decode("#bbbbbb"),
			Color.decode("#333333"),
			null, null, null
		) {
			private BufferedImage bg = getResourceImage("/resources/button.png");
			
			@Override
			public Border createBorder() {
				return null;
			}
			
			@Override
			public void paintBackground(BaseButton bt, Graphics g) {
				int w = bt.getWidth(), h = bt.getHeight();
				
				int y = bt.getModel().isPressed() ? 46 : bt.getModel().isRollover() ? 23 : 0;
				g.drawImage(bg, 1, 1, w - 1, h - 1, 0, y, w - 1, y + 23, null);
				
				g.setColor(borderColor);
				g.drawRoundRect(0, 0, bt.getWidth() - 1, bt.getHeight() - 1, 2, 2);
			}
		};
		
		public Color borderColor, textColor, bgColor, bgHoverColor, bgActiveColor;
		
		ButtonStyle(Color border, Color text, Color bg, Color bgHover, Color bgActive) {
			borderColor = border;
			textColor = text;
			
			bgColor = bg; bgHoverColor = bgHover; bgActiveColor = bgActive;
		}
		
		public Border createBorder() {
			return BorderFactory.createMatteBorder(1, 1, 1, 1, borderColor);
		}
		
		public void paintBackground(BaseButton bt, Graphics g) {
			g.setColor(bt.getModel().isPressed() ? bgActiveColor : bt.getModel().isRollover() ? bgHoverColor : bgColor);
			g.fillRect(0, 0, bt.getWidth(), bt.getHeight());
		}
	}
	
	private ButtonStyle style;
	
	public BaseButton(ButtonStyle s) {
		this(null, null, s);
	}
	
	public BaseButton(String text, ButtonStyle s) {
		this(text, null, s);
	}
	
	public BaseButton(String text, Icon icon, ButtonStyle s) {
		super(text, icon);
		style = s;
		
		this.setContentAreaFilled(false);
		this.setFocusPainted(false);
		
		this.setFont(new Font("Verdana", Font.PLAIN, 11));
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		this.setBorder(style.createBorder());
		this.setForeground(style.textColor);
	}
	
	@Override
	public void paint(Graphics g) {
		style.paintBackground(this, g);
		super.paint(g);
	}
	
	@Override
	public void setSize(int w, int h) {
		this.setMaximumSize(new Dimension(w, h));
		this.setPreferredSize(new Dimension(w, h));
		
		super.setSize(w, h);
	}
	
	private static BufferedImage getResourceImage(String path) {
		try {
			return ImageIO.read(BaseButton.class.getResourceAsStream(path));
		} catch(Exception e) {
			return null;
		}
	}
	
}
