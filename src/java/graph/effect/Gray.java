package graph.effect;

import java.awt.image.BufferedImage;

public class Gray implements IEffect {

	@Override
	public void apply(BufferedImage image, String parameter) {
		for(int x = 0; x < image.getWidth(); x++)
			for(int y = 0; y < image.getHeight(); y++) {
				int color = image.getRGB(x, y);

				int r = (color >> 16) & 0xFF;
				int g = (color >> 8) & 0xFF;
				int b = (color) & 0xFF;
				
				int gray = (r + g + b) / 3;
				
				int clr = (gray << 16) + (gray << 8) + (gray);
				image.setRGB(x, y, clr);
			}
	}

}
