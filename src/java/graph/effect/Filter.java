package graph.effect;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class Filter implements IEffect {

    public enum Type {GAUSS, MEDIAN, HARSHNESS}

    private final Type current;

    public Filter(Type t) {
        current = t;
    }

    @Override
    public void apply(BufferedImage image, String arg) {
        switch (current) {
            case GAUSS: {
                int r = 2;
                double sigma = Double.parseDouble(arg);

                double A = 0;
                for (int l = -r; l <= r; l++)
                    for (int k = -r; k <= r; k++) A += Math.exp(-(l * l + k * k) / (2 * sigma * sigma));
                A = 1 / A;

                double[][] matrix = new double[r * 2 + 1][r * 2 + 1];
                for (int i = -r; i <= r; i++)
                    for (int j = -r; j <= r; j++)
                        matrix[i + r][j + r] = A * Math.exp(-(i * i + j * j) / (2 * sigma * sigma));

                filter(image, r, 0, (x, y) -> matrix[x + r][y + r]);
                break;
            }
            case MEDIAN: {
                filter(image, Integer.parseInt(arg), 0, null);
                break;
            }
            case HARSHNESS: {
                int k = Integer.parseInt(arg);
                int r = 1;

                filter(image, r, 0, (x, y) -> {
                    if (x == r && y == r) return k + 1;
                    return -k / 8D;
                });
                break;
            }
        }
    }

    public static void filter(BufferedImage image, int k, int p, WeightFunction weight) {
        Color[][] colors = new Color[image.getWidth()][image.getHeight()];
        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++)
                colors[x][y] = parseColor(image.getRGB(x, y));

        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++) {
                double r = 0, g = 0, b = 0;

                if (weight != null) {
                    for (int dx = -k; dx <= k; dx++)
                        for (int dy = -k; dy <= k; dy++) {
                            Color c = getPixelColor(image, colors, x + dx, y + dy);

                            double w = weight.getWeightForPixel(dx, dy);
                            r += c.getRed() * w;
                            g += c.getGreen() * w;
                            b += c.getBlue() * w;
                        }
                } else {
                    int q = k * 2 + 1, avg = q * q / 2 - 1;
                    int[][] med = new int[3][q * q];

                    for (int dx = -k; dx <= k; dx++)
                        for (int dy = -k; dy <= k; dy++) {
                            Color c = getPixelColor(image, colors, x + dx, y + dy);
                            int index = (dx + k) * q + (dy + k);

                            med[0][index] = c.getRed();
                            med[1][index] = c.getGreen();
                            med[2][index] = c.getBlue();
                        }

                    for (int i = 0; i < 3; i++) Arrays.sort(med[i]);
                    r = med[0][avg];
                    g = med[1][avg];
                    b = med[2][avg];
                }

                Color c = new Color(normalize(r + p), normalize(g + p), normalize(b + p));
                image.setRGB(x, y, c.getRGB());
            }
    }

    private static Color getPixelColor(BufferedImage image, Color[][] colors, int x, int y) {
        if (x < 0) x = image.getWidth() - x;
        if (y < 0) y = image.getHeight() - y;
        return colors[x % image.getWidth()][y % image.getHeight()];
    }

    private static int normalize(double d) {
        return Math.max(0, Math.min(255, (int) Math.round(d)));
    }

    public interface WeightFunction {

        double getWeightForPixel(int dx, int dy);

    }

    public static Color parseColor(int color) {
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color) & 0xFF;

        return new Color(r, g, b);
    }

}
