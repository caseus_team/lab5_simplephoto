package graph.effect;

import java.awt.image.BufferedImage;

public class Binarization implements IEffect {
	
	@Override
	public void apply(BufferedImage image, String parameter) {
		int k = Integer.parseInt(parameter);
		
		for(int x = 0; x < image.getWidth(); x++)
			for(int y = 0; y < image.getHeight(); y++) {
				int color = image.getRGB(x, y);
				
				int r = (color >> 16) & 0xFF;
				int g = (color >> 8) & 0xFF;
				int b = (color) & 0xFF;
				
				int l = (int)Math.round(0.299 * r + 0.5876 * g + 0.114 * b);
				
				int nc = l < k ? 0 : 0xFFFFFF;
				image.setRGB(x, y, nc);
			}
	}
	
}
