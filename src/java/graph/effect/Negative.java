package graph.effect;

import java.awt.image.BufferedImage;

public class Negative implements IEffect {

	@Override
	public void apply(BufferedImage image, String parameter) {
		for(int x = 0; x < image.getWidth(); x++)
			for(int y = 0; y < image.getHeight(); y++) {
				int color = image.getRGB(x, y);

				int r = (color >> 16) & 0xFF;
				int g = (color >> 8) & 0xFF;
				int b = (color) & 0xFF;
				
				r = 255 - r;
				g = 255 - g;
				b = 255 - b;
				
				int clr = (r << 16) + (g << 8) + (b);
				image.setRGB(x, y, clr);
			}
	}

}
