package graph.effect;

import java.awt.image.BufferedImage;

public interface IEffect {
	
	void apply(BufferedImage image, String parameter);
	
}
