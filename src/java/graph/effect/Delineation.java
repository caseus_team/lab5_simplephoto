package graph.effect;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Delineation implements IEffect {

    private static final Random rand = new Random();

    @Override
    public void apply(BufferedImage image, String arg) {
        (new Filter(Filter.Type.GAUSS)).apply(image, "0.7");
        delineation(image, Integer.parseInt(arg));
    }

    private void delineation(BufferedImage image, int base) {
        double[][] colors = new double[image.getWidth()][image.getHeight()];
        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++) {
                Color c = parseColor(image.getRGB(x, y));
                colors[x][y] = c.getRed() * 0.299 + c.getGreen() * 0.587 + c.getBlue() * 0.114;
            }

        int k = 1;
        double[][] m1 = new double[][]{
                { 1, 2, 1 },
                { 0, 0, 0 },
                { -1, -2, -1 }
        };
        double[][] m2 = new double[][]{
                { -1, 0, 1 },
                { -2, 0, 2 },
                { -1, 0, 1 }
        };

        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++) {
                double a = 0, b = 0;

                for (int dx = -k; dx <= k; dx++)
                    for (int dy = -k; dy <= k; dy++) {
                        a += m1[dx + k][dy + k] * getPixelColor(image, colors, x + dx, y + dy);
                        b += m2[dx + k][dy + k] * getPixelColor(image, colors, x + dx, y + dy);
                    }

                double g = Math.sqrt(a * a + b * b);
                image.setRGB(x, y, g < base ? 0 : 0xFFFFFF);
            }
    }

    private void distortion(BufferedImage image, MapFunction fx, MapFunction fy) {
        BufferedImage copy = createCopy(image);

        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++) {
                int nx = Math.max(0, Math.min(fx.map(x, y), image.getWidth() - 1));
                int ny = Math.max(0, Math.min(fy.map(x, y), image.getHeight() - 1));

                image.setRGB(x, y, copy.getRGB(nx, ny));
            }
    }

    public static BufferedImage createCopy(BufferedImage source) {
        BufferedImage copy = createSizeCopy(source);
        copy.getGraphics().drawImage(source, 0, 0, source.getWidth(), source.getHeight(), null);
        return copy;
    }

    public static BufferedImage createSizeCopy(BufferedImage source) {
        return new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);
    }

    private static double getPixelColor(BufferedImage image, double[][] colors, int x, int y) {
        if (x < 0) x = image.getWidth() - x;
        if (y < 0) y = image.getHeight() - y;
        return colors[x % image.getWidth()][y % image.getHeight()];
    }

    private interface MapFunction {
        int map(int x, int y);
    }

    public static Color parseColor(int color) {
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color) & 0xFF;

        return new Color(r, g, b);
    }
}
