package graph.effect;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Noise implements IEffect {
	
	private static final Random rand = new Random();
	private final boolean lines;
	
	public Noise(boolean l) {
		lines = l;
	}
	
	@Override
	public void apply(BufferedImage image, String arg) {
		if(!lines) {
			for(int x = 0; x < image.getWidth(); x++)
				for(int y = 0; y < image.getHeight(); y++) {
					Color c = parseColor(image.getRGB(x, y));
					
					c = new Color(addNoize(c.getRed()), addNoize(c.getGreen()), addNoize(c.getBlue()));
					image.setRGB(x, y, c.getRGB());
				}
		} else {
			Graphics2D g = (Graphics2D)image.getGraphics();
			
			for(int i = 0; i < 50; i++) {
				g.setColor(randomColor());
				
				int x = rand.nextInt(image.getWidth()), y = rand.nextInt(image.getHeight());
				int w = rand.nextInt(30), h = rand.nextInt(30);
				
				g.drawLine(x, y, x + w, y + h);
			}
			
			for(int i = 0; i < 20; i++) {
				g.setColor(randomColor());

				int x = rand.nextInt(image.getWidth()), y = rand.nextInt(image.getHeight());
				int d = rand.nextInt(30);
				
				g.drawOval(x, y, d, d);
			}
		}
	}
	
	private int addNoize(int v) {
		int p = (int)(256D / 100 * 12.5);
		return Math.max(0, Math.min(v + (rand.nextInt(p * 2) - p), 255));
	}
	
	private Color randomColor() {
		return new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
	}

	public static Color parseColor(int color) {
		int r = (color >> 16) & 0xFF;
		int g = (color >> 8) & 0xFF;
		int b = (color) & 0xFF;

		return new Color(r, g, b);
	}
	
}
