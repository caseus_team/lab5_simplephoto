package graph.effect;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Histogram {
	
	private int[] all, red, green, blue;
	public enum Channel { 
		ALL("#777777"), RED("#e98a8a"), GREEN("#93d89a"), BLUE("#8cb4eb");
		
		protected final Color color;
		Channel(String c) { color = Color.decode(c); }
	}
	
	public void update(BufferedImage image) {
		all = new int[256]; red = new int[256]; green = new int[256]; blue = new int[256];
		for(int x = 0; x < image.getWidth(); x++)
			for(int y = 0; y < image.getHeight(); y++) {
				int color = image.getRGB(x, y);
				
				int r = (color >> 16) & 0xFF;
				int g = (color >> 8) & 0xFF;
				int b = (color) & 0xFF;
				
				all[(int)Math.round(0.299 * r + 0.5876 * g + 0.114 * b)]++;
				red[r]++; green[g]++; blue[b]++;
			}
	}
	
	public BufferedImage draw(Channel c) {
		int[] data = null;
		switch(c) {
			case ALL:	data = all;		break;
			case RED:	data = red;		break;
			case GREEN:	data = green;	break;
			case BLUE:	data = blue;	break;
		}
		
		int max = 0, cnt = 0;
		for(int val : data) {
			max = Math.max(max, val);
			cnt += val;
		}
		max = Math.min(max, (int)(cnt / 256D * 8));
		
		BufferedImage image = new BufferedImage(256 + 2, 100 + 2, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D)image.getGraphics();
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, image.getWidth(), image.getHeight());
		
		g.setColor(Color.GRAY);
		g.drawRect(0, 0, image.getWidth() - 1, image.getHeight() - 1);
		
		g.setColor(c.color);
		for(int i = 0; i < data.length; i++) {
			int val = (int)Math.round(data[i] * 1D * (image.getHeight() - 2) / max);
			g.drawLine(i + 1, Math.max(1, image.getHeight() - val - 2), i + 1, image.getHeight() - 2);
		}
		
		return image;
	}
	
}
