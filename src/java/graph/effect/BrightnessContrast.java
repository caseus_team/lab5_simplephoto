package graph.effect;

import java.awt.image.BufferedImage;

public class BrightnessContrast {
	
	public static void apply(BufferedImage source, BufferedImage target, int brightness, int contrast) {
		double ra = 0, ga = 0, ba = 0;


		for(int x = 0; x < source.getWidth(); x++) {
			for(int y = 0; y < source.getHeight(); y++) {
				int color = source.getRGB(x, y);
				
				int r = (color >> 16) & 0xFF;
				int g = (color >> 8) & 0xFF;
				int b = (color) & 0xFF;
				
				ra += r; ga += g; ba += b;
			}
		}

        int size = source.getWidth() * source.getHeight();
        ra /= size * 1.0;
		ga /= size * 1.0;
		ba /= size * 1.0;

        if (contrast == 0) {
            contrast = 50;
        }
        else {
            if (contrast > 0) {
                contrast = (int)(contrast * 50 / 255.0) + 50;
            }
            else {
                contrast = (int)((255 - (contrast * -1)) * 50 / 255.0);
            }
        }

        double k;
        if (contrast >= 50)
        {
            k = 1 + ((contrast - 50) / 50.0) * 19;
        }
        else
        {
            k = 0.05 + (contrast / 50.0) * 0.85;
        }
		
		int[] replaceR = new int[256];
		int[] replaceG = new int[256];
		int[] replaceB = new int[256];

		for(int i = 0; i < replaceR.length; i++) {
			
			int r = ((int) Math.round(k * (i - ra) + ra));
			int g = ((int) Math.round(k * (i - ga) + ga));
			int b = ((int) Math.round(k * (i - ba) + ba));
			replaceR[i] = normalize(r);
			replaceG[i] = normalize(g);
			replaceB[i] = normalize(b);
		}
		
		for(int x = 0; x < source.getWidth(); x++) {
			for(int y = 0; y < source.getHeight(); y++) {
				int color = source.getRGB(x, y);

				int r = (color >> 16) & 0xFF;
				int g = (color >> 8) & 0xFF;
				int b = (color) & 0xFF;

				color = (normalize(replaceR[r] + brightness) << 16) + (normalize(replaceG[g] + brightness) << 8) + normalize(replaceB[b] + brightness);
				target.setRGB(x, y, color);
			}
		}
	}
	
	private static int normalize(int x) {
		return Math.max(0, Math.min(x, 255));
	}
	
}
