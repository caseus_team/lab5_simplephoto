package graph.controller;

import graph.Pair;
import graph.effect.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static graph.effect.Histogram.Channel.*;

public class EditorForm {

    public Slider brightnessSlider;
    public Slider contrastSlider;
    public TextField binValueTextField;
    public AnchorPane histogramPane;
    public ImageView imageView;

    public ImageView allHistogram;
    public ImageView redHistogram;
    public ImageView greenHistogram;
    public ImageView blueHistogram;
    public ImageView selectedRectImageView;
    public Rectangle selectedRectangle;
    public TextField gaussTextField;
    public TextField medianTextField;
    public Slider delineationSlider;
    public TextField harshnessTextField;

    private Map<Histogram.Channel, ImageView> histogramMap;

    private BufferedImage original, image, part, renderPart;
    private File file;
    private Histogram histogram = new Histogram();
    private Pair<Integer, Integer> selA, position = null;
    private int brightnessDiff, contrastDiff;


    @FXML
    private void initialize() {
        openImageChooser();
        clearSelectedRectangle();
        medianTextField.setText("2");
        gaussTextField.setText("0.5");
        harshnessTextField.setText("2");
        initSlider(delineationSlider, 0, 256, 128, 32);
        delineationSlider.setValue(64);
        imageView.setFitHeight(image.getHeight());
        imageView.setFitWidth(image.getWidth());
        initSlider(brightnessSlider, -255, 255, 64, 32);
        brightnessSlider.setValue(0);
        brightnessSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            brightnessDiff = new_val.intValue();
            updateAll();
            //brightnessDiff = 0;
        });
        initSlider(contrastSlider, -255, 255, 64, 32);
        contrastSlider.setValue(0);
        contrastSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            contrastDiff = new_val.intValue();
            updateAll();
            //contrastDiff = 0;
        });
        binValueTextField.setText("128");
        EventHandler<MouseEvent> mousePressedEventHandler = event -> {
            applyChangesAndResetSliders();
            selA = Pair.of(((int) (event.getX())) , ((int) (event.getY())));
        };
        imageView.onMousePressedProperty().setValue(mousePressedEventHandler);
        selectedRectImageView.onMousePressedProperty().setValue(mousePressedEventHandler);
        EventHandler<MouseEvent> mouseDraggedEventHandler = event -> {
            int x = Math.max(0, Math.min(((int) (event.getX())), image.getWidth() - 1));
            int y = Math.max(0, Math.min(((int) (event.getY())), image.getHeight() - 1));
            int x1 = Math.min(selA.left, x), x2 = Math.max(selA.left, x);
            int y1 = Math.min(selA.right, y), y2 = Math.max(selA.right, y);
            if (x1 != x2 && y1 != y2) {
                position = Pair.of(x1, y1);
                part = image.getSubimage(x1, y1, x2 - x1, y2 - y1);
                renderPart = createCopy(part);
                selectedRectangle.setX(x1);
                selectedRectangle.setY(y1);
                selectedRectangle.setWidth(x2 - x1);
                selectedRectangle.setHeight(y2 - y1);
                updateAll();
            }
        };
        imageView.onMouseDraggedProperty().setValue(mouseDraggedEventHandler);
        selectedRectImageView.onMouseDraggedProperty().setValue(mouseDraggedEventHandler);
        histogramMap = new HashMap<>();
        histogramMap.put(ALL, allHistogram);
        histogramMap.put(RED, redHistogram);
        histogramMap.put(GREEN, greenHistogram);
        histogramMap.put(BLUE, blueHistogram);
        updateHistogram();
        BrightnessContrast.apply(part, renderPart, 0, 50);
        drawImage();
    }

    private void clearSelectedRectangle() {
        selectedRectangle.setFill(Color.TRANSPARENT);
        selectedRectangle.setHeight(0);
        selectedRectangle.setWidth(0);
    }

    private void initSlider(Slider slider, double min, double max, double setMajorTickSpacing, int setMinorTickCount) {
        slider.setMin(min);
        slider.setMax(max);
        slider.setValue(0);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(setMajorTickSpacing);
        slider.setMinorTickCount(setMinorTickCount);
        slider.setBlockIncrement(10);
    }

    private void update() {
        imageView.setImage(new Image(file.toURI().toString()));
        imageView.setVisible(true);
        histogram.update(part);
        drawImage();
    }

    private void drawImage() {
        if(brightnessSlider != null)
            BrightnessContrast.apply(part, renderPart, brightnessDiff, contrastDiff);

        if(position != null) {
            imageView.setImage(SwingFXUtils.toFXImage(image, null));
        }

        int x = position == null ? 0 : position.left;
        int y = position == null ? 0 : position.right;
        selectedRectImageView.setX(x);
        selectedRectImageView.setY(y);
        selectedRectImageView.setFitWidth(renderPart.getWidth());
        selectedRectImageView.setFitHeight(renderPart.getHeight());
        selectedRectImageView.setImage(SwingFXUtils.toFXImage(renderPart, null));
    }

    private void openImageChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        fileChooser.setInitialDirectory(new File("resources/images"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            file = selectedFile;
            try {
                image = part = ImageIO.read(selectedFile);
                original = createCopy(image);
                renderPart = createCopy(part);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
        update();
    }

    public void onBinButtonClick() {
        applyEffect(new Binarization(), binValueTextField.getText());
    }

    public void onGreyClick() {
        applyEffect(new Gray(), null);
    }

    public void onNegativeClick() {
        applyEffect(new Negative(), null);
    }

    public void onSelectButtonClick() {
        imageView.setVisible(false);
        openImageChooser();
    }

    public void onRemoveSelectionButtonClick() {
        resetSelection();
        updateAll();
    }

    public void onClearButtonClick() {
        resetSelection();
        part = image = createCopy(original);
        renderPart = createCopy(part);
        updateAll();
    }

    private void updateAll() {
        histogram.update(renderPart);
        imageView.setImage(SwingFXUtils.toFXImage(part, null));
        updateHistogram();
        drawImage();
    }

    private void updateHistogram() {
        for (Histogram.Channel channel : Histogram.Channel.values()) {
            BufferedImage img = histogram.draw(channel);
            ImageView histogramImageView = histogramMap.get(channel);
            histogramImageView.setImage(SwingFXUtils.toFXImage(img, null));
            histogramImageView.setFitWidth(img.getWidth());
            histogramImageView.setFitHeight(img.getHeight());
        }
    }

    private void applyEffect(IEffect effect, String parameter) {
        effect.apply(part, parameter);
        updateAll();
    }

    private BufferedImage createCopy(BufferedImage source) {
        BufferedImage copy = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);
        copy.getGraphics().drawImage(source, 0, 0, source.getWidth(), source.getHeight(), null);
        return copy;
    }

    private void resetSelection() {
        clearSelectedRectangle();
        applyChangesAndResetSliders();
        if (position != null) {
            position = null;
            part = image;
            renderPart = createCopy(part);
        }
    }

    private void applyChangesAndResetSliders() {
        part.getGraphics().drawImage(renderPart, 0, 0, part.getWidth(), part.getHeight(), null);
        brightnessSlider.setValue(0);
        contrastSlider.setValue(0);
    }

    public void onPointNoiseClick() {
        applyEffect(new Noise(false), null);
    }

    public void onLineNoiseClick() {
        applyEffect(new Noise(true), null);
 }

    public void onMedianClick() {
        applyEffect(new Filter(Filter.Type.MEDIAN), medianTextField.getText());
}

    public void onGaussClick() {
        applyEffect(new Filter(Filter.Type.GAUSS), gaussTextField.getText());
}

    public void onHarshnessClick() {
        applyEffect(new Filter(Filter.Type.HARSHNESS), harshnessTextField.getText());
}

    public void onDelineationClick() {
        applyEffect(new Delineation(), String.valueOf(((int)delineationSlider.getValue())));
    }
}
